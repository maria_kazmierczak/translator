class CezarCodeClass(object):

    def crypt(self, txt, key):
        if txt == None or key == 0 or type(key) == str:
            return None
        else:
            txtcryp = ""
            for i in range(len(txt)):
                if ord(txt[i]) > 122 - key:
                    txtcryp += chr(ord(txt[i]) + key - 26)
                else:
                    txtcryp += chr(ord(txt[i]) + key)
            return txtcryp