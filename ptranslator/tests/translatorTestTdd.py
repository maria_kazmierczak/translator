# To jest miejsce od którego należy zacząć zadanie TDD
import pytest
import unittest
from tests.CezarCode import CezarCodeClass

class TestExist(unittest.TestCase):
    def testExistClassCezar(self):
        cezar=CezarCodeClass()

class TestCezarCode(unittest.TestCase):
    cezar = CezarCodeClass()
    def testNullTxt(self):
        self.assertEqual(None, self.cezar.crypt(None,3))

class TestCezarCode2(unittest.TestCase):

    def testTxt(self):
        self.assertEqual("slhv",self.cezar.crypt("pies",3))

class TestCezarCode3(unittest.TestCase):
    cezar = CezarCodeClass()
    def testNullkey(self):
        self.assertEqual(None, self.cezar.crypt("pies",0))

class TestCezarCode4(unittest.TestCase):
    cezar = CezarCodeClass()
    def teststringkey(self):
        self.assertEqual(None, self.cezar.crypt("pies","a"))

@pytest.mark.parametrize("shift, test_input, expected", [
    (2,"pies","rkgu"),
    (28,"pies","rkgu"),
    (34,"pies","xqm{"),
    (0, "pies", None)
])

def test_ceasar_crypt(shift, test_input, expected):
    cezar = CezarCodeClass()
    assert cezar.crypt(test_input,shift)==expected